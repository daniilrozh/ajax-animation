$(function() {

  var $body = $('body'),
      $loadLink = $('.menu__link'),
      preloadImages = function(images) {
        images.map(function(image) {
          $("<img />").attr("src", image);
        });
      },
      images = [
        '/images/cool-girl.jpg',
        '/images/surfer.jpg',
        '/images/italy.jpg',
        '/images/lighthouse.jpg',
        '/images/gallery/gallery_01.jpg',
        '/images/gallery/gallery_02.jpg',
        '/images/gallery/gallery_03.jpg',
        '/images/gallery/gallery_04.jpg'
      ];


  // TweenMax Scroll Effect
  var $window = $(window);
  var scrollTime = 1;
  var scrollDistance = 320;

  $window.on("mousewheel DOMMouseScroll", function(event){

    event.preventDefault();

    var delta = event.originalEvent.wheelDelta/120 || -event.originalEvent.detail/3;
    var scrollTop = $window.scrollTop();
    var finalScroll = scrollTop - parseInt(delta*scrollDistance);

    TweenMax.to($window, scrollTime, {
      scrollTo : { y: finalScroll, autoKill:true },
      ease: Power1.easeOut,
      overwrite: 5
    });

  });

  var wow = new WOW(
    {
      boxClass:     'js-visible',
      animateClass: 'is-visible',
      offset: 0,
      mobile: true,
      live: true
    }
  );
  wow.init();

  var paceOptions = {
    elements: false,
    restartOnRequestAfter: false
  };

  $body.addClass('is-loaded');
  preloadImages(images);

  $loadLink.click(function(e) {
    e.preventDefault();

    $loadLink.removeClass('is-current');
    $(this).addClass('is-current');

    var $link = $(this).attr('href'),
        $content;

    if( $link != window.location.pathname ) {
      window.history.pushState({ path: $link }, '', $link);
    } else {
      return false;
    }

    $loadLink.attr('disabled', 'disabled');
    $body.removeClass('is-loaded');
    $('.js-visible').removeClass('is-visible');

    var deferredAjax = $.Deferred();
    var ajaxResponse = function() {
      $.ajax({
        url: '/pages/' + $link,
        success: function(data) {
          $content = data;
          deferredAjax.resolve();
        },
        error: function(data) {
          $content = 'Nothing';
        }
      });
    };
    ajaxResponse();

    var deferredAnimation = $.Deferred();
    var animationEnd = function() {
      $body.one(
        'transitionend',
        '.header__title',
        function() {
          deferredAnimation.resolve();
        });
    };
    animationEnd();

    var complete = $.when(deferredAjax, deferredAnimation);
    complete.done(function() {
      $('.container').html($content);

      setTimeout(function(){
        $body.addClass('is-loaded');
        $loadLink.removeAttr('disabled');
      }, 100);
    });

  });

  $(window).on('popstate', function() {
    var newPageArray = location.pathname.split('/'),
        newPage = newPageArray[newPageArray.length - 1],
        $content;

    if (newPage == '') {
      window.location.reload();
      return false;
    }

    $body.removeClass('is-loaded');
    $('.js-visible').removeClass('is-visible');
    $loadLink.removeClass('is-current');
    $('[href = "/' + newPage + '"]').addClass('is-current');

    var deferredAjax = $.Deferred();
    var ajaxResponse = function() {
      $.ajax({
        url: '/pages/' + newPage,
        success: function(data) {
          $content = data;
          deferredAjax.resolve();
        },
        error: function(data) {
          $content = 'Nothing';
        }
      });
    };
    ajaxResponse();

    var deferredAnimation = $.Deferred();
    var animationEnd = function() {
      $body.one(
        'transitionend',
        '.header__title',
        function() {
          deferredAnimation.resolve();
        });
    };
    animationEnd();

    var complete = $.when(deferredAjax, deferredAnimation);
    complete.done(function() {
      $('.container').html($content);

      setTimeout(function(){
        $body.addClass('is-loaded');
        $loadLink.removeAttr('disabled');
      }, 100);
    });

  });


  // Modals
  $(document).on('click', '.modal', function() {
    $(this).toggleClass('is-visible');
  });

  $(document).on('click', '.js-modal', function() {
    var image = $(this)
      .find('img')
      .attr('src');
    $('.modal__content')
      .html(
        $('<img class="modal__image">')
          .attr('src', image)
      );
    $('.modal').toggleClass('is-visible');
  });

});
