$(function() {

  TweenLite.fromTo(
    $('.animate'),
    .7,
    {
      opacity: 0,
      scale: .7
    },
    {
      scale: 1,
      opacity: 1,
      ease: Quad.easeInOut
    }
  );

  var $animate = $('.animate__item');
  var $fire = $('.fire-button');
  var $fireWave = $('.fire-button__wave');

  $animate.click(function() {
    return false;
  });

  var scaleAnimation = function(self) {
    TweenLite.fromTo(
      self,
      .5,
      {
        scale: .95,
      },
      {
        scale: 1,
        onComplete: function() {
          console.log(self.attr('href'));
        }
      }
    );
  }

  var pushAnimation = function(self) {
    TweenLite.fromTo(
      self,
      .5,
      {
        scale: .95,
        rotationX: -1
      },
      {
        rotationX: 0,
        scale: 1,
        ease: Power4.easeIn,
        onComplete: function() {
          console.log(self.attr('href'));
        }
      }
    );
  };

  $animate.on('touchstart mousedown', function() {

    var self = $(this);

    TweenLite.set(self, {
      transformPerspective: 200
    });

    self.css({
      transition: 'all .35s cubic-bezier(0.13, 0.82, 0.35, 0.96)'
    });

    switch (self.attr('data-animate')) {
      case 'scale':
        scaleAnimation(self);
        break;
      case 'push':
        pushAnimation(self);
        break;
      default:
        return false;
    }

    return false;
  });

  $fire.on('touchstart mousedown', function() {

    var self = $(this);

    TweenLite.set(self, {
      transformPerspective: 200
    });

    self.css({
      transition: 'all .35s cubic-bezier(0.13, 0.82, 0.35, 0.96)'
    });

    // $fireWave.css({
    //   transition: 'all .35s cubic-bezier(0.13, 0.82, 0.35, 0.96)'
    // });

    TweenLite.fromTo(
      self,
      .5,
      {
        scale: .85,
      },
      {
        scale: 1,
        onComplete: function() {
          console.log(self.attr('href'));
        }
      }
    );

    TweenLite.to(
      $fireWave,
      .5,
      {
        scale: 3,
        opacity: 0,
        ease: Power4.easeOut,
        onComplete: function() {
          console.log('Ready');
          TweenLite.set(
            $fireWave,
            {
              scale: 1,
              opacity: 1
            }
          );
        }
      }
    );

    return false;
  });

});
